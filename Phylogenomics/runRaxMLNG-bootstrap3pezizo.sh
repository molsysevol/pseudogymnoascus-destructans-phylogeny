#!/bin/bash

#SBATCH --job-name=wns-raxml-bs3pezizo
#SBATCH --ntasks=32 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=502G 
#SBATCH --error=cluster_log_bootstraps3p.%J.err
#SBATCH --output=cluster_log_bootstraps3p.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 
#SBATCH --array=1-100

./raxml-ng \
  --msa SuperAlignment3.sampled03.pezizo.fasta.raxml.rba \
  --prefix Bootstrap3p/SuperAlignment3.sampled03.pezizo.fasta.raxml.bootstrap${SLURM_ARRAY_TASK_ID} \
  --bootstrap \
  --bs-trees 1 \
  --seed $((SLURM_ARRAY_TASK_ID + 42)) \
  --threads 32 \
  
