#!/bin/bash

#SBATCH --job-name=wns-raxml
#SBATCH --ntasks=32 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=300G 
#SBATCH --error=cluster_log.%J.err
#SBATCH --output=cluster_log.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 


./raxml-ng --threads 32 --msa SuperAlignment.fasta.raxml_nopart.rba --prefix SuperAlignment.raxml_nopart --seed 42

