#!/bin/bash

#SBATCH --job-name=wns-fasttree3
#SBATCH --ntasks=1 # num of cores
#SBATCH --nodes=1
#SBATCH --time=100:00:00 # in hours
#SBATCH --mem=500G 
#SBATCH --error=fastree_log3.%J.err
#SBATCH --output=fastree_log3.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard


FastTree -lg < SuperAlignment3.fasta > SuperAlignment3.fasttree.dnd

