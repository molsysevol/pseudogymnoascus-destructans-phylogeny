#!/bin/bash

#SBATCH --job-name=wns-raxml3
#SBATCH --ntasks=6 # num of cores
#SBATCH --nodes=1
#SBATCH --time=72:00:00 # in hours
#SBATCH --mem=4G 
#SBATCH --error=cluster_log3.%J.err
#SBATCH --output=cluster_log3.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 
#SBATCH --array=1-556 

GENE=`sed "${SLURM_ARRAY_TASK_ID}q;d" AllGenes.txt`
mkdir -p Phylogenies_raxml/$GENE
./raxml-ng --threads 6 --msa Alignments_filtered_renamed/$GENE.aln_filtered_renamed.fasta --model LG+G --prefix Phylogenies_raxml/$GENE/$GENE --seed $((SLURM_ARRAY_TASK_ID + 42))


