#! /usr/bin/python

# Split the blast output per query gene

import gzip

with gzip.open('BlastOut/matches.out.gz', 'rt') as handle:
    current_query = ""
    output_handle = None
    count = 0
    for line in handle:
        values = line.split("\t")
        query = values[0]
        if query != current_query:
            count = count + 1
            print("Create new file for %s (# %d)" % (query, count))
            #Create a new file
            if output_handle != None:
                output_handle.close()
            output_handle = gzip.open("BlastOut/Split/%s.out.gz" % query, 'wt')
        #Otherwise, append existing file
        
        output_handle.write(line)
        #Next
        current_query = query

    output_handle.close()
