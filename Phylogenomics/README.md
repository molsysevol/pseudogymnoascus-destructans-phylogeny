<!-- Created on 17.09.20 -->

Phylogenomic analyses of *Pseudogymnoascus destructans* (Pd).

# Reconstruction of gene families

We 'blast' each protein from the Pd gene catalog on the non-redundant (nr) protein database.
Blast+ is too slow so we use Diamond instead.

We retrieve all ncbi sequences (25/09/20).
As the file is very large, we use lftp, with the command pget -n 40
```bash
lftp ftp://ftp.ncbi.nlm.nih.gov/
cd blast/db/FASTA/
pget -n 40 nr.gz
pget nr.gz.md5
```
Check file integrity:
```bash
md5sum -c nr.gz.md5
```
Get taxonomic ids:
```bash
lftp ftp://ftp.ncbi.nlm.nih.gov/
cd pub/taxonomy/
pget -n 40 taxdmp.zip
pget taxdmp.zip.md5

cd accession2taxid/
pget -n 40 prot.accession2taxid.gz
pget prot.accession2taxid.gz.md5
```
Check file integrity:
```bash
md5sum -c prot.accession2taxid.gz.md5
md5sum -c taxdmp.zip.md5
unzip taxdmp.zip
```

We generate a diamond database:
```bash
diamond makedb \
          --in nr.gz \
          --db nr \
          --taxonmap prot.accession2taxid.gz \
          --taxonnodes nodes.dmp \
          --taxonnames names.dmp
```
Version 2.0.4.142, complete taxonomy:
```
Database hash = 7591ccd3104a90b94c600708d43e237c
Processed 316065422 sequences, 113949932586 letters.
Total time = 6807.26s
```
Then we 'blast' all *P. destructans* sequences. We use the -b4 options as suggested when running without:
```
The host system is detected to have 33 GB of RAM. It is recommended to increase the block size for better performance using these parameters : -b4
```
We restrict the search to fungi (taxid:4751). 
```bash
diamond blastp \
          -d nr \
          -q ../Genome/Pseudest1_GeneCatalog_proteins_20170919.aa.fasta \
          -e 1e-6 -k0 -b4 \
          --taxonlist 4751 \
          -f 6 qseqid sseqid qlen slen pident nident evalue staxids sscinames sskingdoms skingdoms sphylums full_sseq \
          -o matches.out

gzip matches.out
```

The output is very large, we split it into one file per query:
```bash
mkdir BlastOut/Split

python3 splitBlastOutput.py
```


We then analyse each entry separately. We keep only the best HSP for each target, and count the targets for each species:
```r
require(plyr)
lst <- list.files("BlastOut/Split/")
counts <- list()
pb <- txtProgressBar(0, length(lst), style = 3)
i <- 0
for (gene in lst) {
  i <- i + 1
  setTxtProgressBar(pb, i)
  tryCatch(dat <- read.table(paste("BlastOut/Split/", gene, sep = ""), sep = "\t", stringsAsFactors = FALSE, comment.char = "", quote = ""), warning = function(x) { stop(paste("Problem with file", gene, "\n")) })
  dat2 <- ddply(dat, .variables = c("V1", "V2"), .fun = function(d) return(d[which.max(d$V6),]))
  # When one sequence belongs to several species, count it for each of them.
  l <- strsplit(dat2$V9, ";")
  tbl <- table(unlist(l))
  df <- as.data.frame(tbl)
  names(df) <- c("Taxon", gene)
  counts[[gene]] <- df
}
save(counts, file="counts.Rdata")

# Then merge all tables:
require(dplyr)
allCounts <- counts[[1]]
pb <- txtProgressBar(0, length(counts) - 1, style = 3)
for (i in 2:length(counts)) {
  setTxtProgressBar(pb, i - 1)
  allCounts <- full_join(allCounts, counts[[i]], by = "Taxon")
}

write.table(allCounts, file=gzfile("GeneCounts.tsv.gz"), sep = "\t", row.names = FALSE)
```

# First approach: maximize the number of genes and species, while minimizing the amount of missing data

## Selecting candidate genes and species
```r
allCounts <- read.table("GeneCounts.tsv.gz", sep = "\t", header = TRUE)
row.names(allCounts) <- allCounts$Taxon
allCounts$Taxon <- NULL

# We remove all self-hist:
pdi <- which(row.names(allCounts) == "Pseudogymnoascus destructans")
allCounts <- allCounts[-pdi,]

# We look at duplicates, per gene and per species:
nbDupGenes <- apply(allCounts, 2, function(x) sum(x > 1, na.rm = TRUE))
nbDupSpecies <- apply(allCounts, 1, function(x) sum(x > 1, na.rm = TRUE))

sum(nbDupGenes == 0) #1132 genes without any duplicate
sum(nbDupSpecies == 0) #23149 species without any duplicate

# We first remove genes with too many duplicates. Such genes most likely represent complex
# gene families, which are not good phylogenetic markers.
hist(nbDupGenes)

# Rarefaction curve:
x <- seq(0, 500, by = 1)
y <- Vectorize(function(x) sum(nbDupGenes <= x))(x)
plot(y~x, type = "l", ylim=c(0,9310))
abline(v=150, col="orange")

# We keep only genes with less than 10 duplicates:
allCounts2 <- allCounts[, nbDupGenes <= 150] # 6272 genes

# Next we optimize missing data, removing species with too little genes:
nbGenes <- apply(allCounts2, 1, function(x) sum(!is.na(x)))

# Rarefaction curve:
x <- seq(1, 2000, by = 1)
y <- Vectorize(function(x) sum(nbGenes >= x))(x)
plot(y~x, type = "l", log="y")
abline(v=1000, col="orange")

# We keep only species with at least 1000 genes:
allCounts3 <- allCounts2[nbGenes >= 1000,] # 1279 species

# We have a second look at genes:
nbSpecies <- apply(allCounts3, 2, function(x) sum(!is.na(x)))

# Rarefaction curve:
x <- seq(1, 1250, by = 1)
y <- Vectorize(function(x) sum(nbSpecies >= x))(x)
plot(y~x, type = "l", log="y")
abline(v=30, col="orange")

# We keep only genes with at least 30 species:
allCounts4 <- allCounts3[,nbSpecies >= 30,] # 4192 genes

# We try to prgressively enrich the data set by sequentially removing species and genes:
propMissing <- function(d) {
  return(sum(is.na(d)) / (nrow(d) * ncol(d)))
}

enrich <- function(d, maxProp) {
  currentProp <- propMissing(d)
  while(currentProp > maxProp) {
    cat(ncol(d), "genes and", nrow(d), "species, current proportion of missing data is", currentProp, "\n")
    # Compute all possible improvements:
    mrow = apply(d, 1, function(x) return(sum(is.na(x))))
    mcol = apply(d, 2, function(x) return(sum(is.na(x))))
    if ((max(mcol) / nrow(d)) > (max(mrow) / ncol(d))) {
      # We remove one gene
      d2 <- d[,-which.max(mcol)]
    } else {
      # We remove one species
      d2 <- d[-which.max(mrow),]
    }
    currentProp <- propMissing(d2)
    d <- d2
  }
  cat(ncol(d), "genes and", nrow(d), "species, current proportion of missing data is", currentProp, "\n")
  return(d)
}

# We ignore duplicated genes:
allCounts5 <- allCounts4
allCounts5[allCounts5 > 1] <- NA
allCounts6 <- enrich(allCounts5, 0.30) #2780 genes and 1126 species, current proportion of missing data is 0.2998326
allCounts7 <- enrich(allCounts6, 0.10) #2753 genes and 667 species, current proportion of missing data is 0.09991009
allCounts8 <- enrich(allCounts7, 0.01) #1493 genes and 424 species, current proportion of missing data is 0.009997915
allCounts9 <- enrich(allCounts8, 0.00) #556 genes and 249 species, current proportion of missing data is 0 

write.table(allCounts9, file=gzfile("GeneCountsSelected.tsv.gz"), sep = "\t", row.names = TRUE)
# Note: basically, this contains only 1! But also the list of genes and species.
```

## Sequence extraction

We now extract each set of sequences:
```bash
mkdir Sequences
```

```r
dat <- read.table(file="GeneCountsSelected.tsv.gz", sep = "\t", check.names = FALSE)
genes <- colnames(dat)
species <- rownames(dat)
require(seqinr)
queries <- read.fasta("../Genome/Pseudest1_GeneCatalog_proteins_20170919.aa.fasta", seqtype = "AA")

for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  seqfile = paste("Sequences/", gname, ".fasta", sep = "")
  cat("Building sequence file for",  gene, "in", seqfile, "\n")
  write.fasta(sequences = queries[gname], names = "Pseudogymnoascus_destructans", file.out = seqfile)

  # Now get, all sequences for other species:
  tryCatch(blastOut <- read.table(paste("BlastOut/Split/", gene, sep = ""), sep = "\t", stringsAsFactors = FALSE, comment.char = "", quote = ""), warning = function(x) { stop(paste("Problem with file", gene, "\n")) })
  l <- strsplit(blastOut$V9, ";")
  # Remove duplicated sequences (as a double check)
  tbl <- table(unlist(l))
  dupSeqs <- names(tbl[tbl > 1])
  sequences <- list()
  for (i in 1:length(l)) {
    for (j in l[[i]]) {
      if (!j %in% dupSeqs) {
        sequences[[j]] <- blastOut[i, 13]
      }
    }
  } #Note: if we wanted to include duplicated sequences, we would need to keep track of the unique seq ids along with taxon ids
  
  # Now extract sequences:
  for (sp in species) {
    if (! sp %in% names(sequences)) {
      stop(paste("Sequence not found :(", sp))
    } else {
      cat(paste(">", gsub(" ", "_", sp, fixed = TRUE), "\n", sequences[[sp]], "\n", sep = ""), file = seqfile, append = TRUE)    
    }
  }
}
```

## Sequence alignment

```r
file <- "cmd_parallel_alignment_muscle.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("muscle -in \"Sequences/", gname, ".fasta\" -out \"Alignments_muscle/", gname, ".aln_muscle.fasta\"\n", sep = "", file = file, append = TRUE)
}
```

Align all genes using muscle
```bash
mkdir Alignments_muscle
parallel -j 20 --eta < cmd_parallel_alignment_muscle.sh
```

```r
file <- "cmd_parallel_alignment_clustalo.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("clustalo -i \"Sequences/", gname, ".fasta\" -o \"Alignments_clustalo/", gname, ".aln_clustalo.fasta\" -v\n", sep = "", file = file, append = TRUE)
}
```
Align all genes using clustalo. We only run one at a time as clustalo is multithreaded and uses all available cores
```bash
mkdir Alignments_clustalo
parallel -j 1 --eta < cmd_parallel_alignment_clustalo.sh
```

Then we compare the two alignments and keep only consistent positions (consensus alignment):
```r
file <- "cmd_parallel_alignment_bppalnscore.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("bppalnscore param=AlnScore.bpp \"DATA=", gname, "\"\n", sep = "", file = file, append = TRUE)
}
```
```bash
mkdir Alignments_consensus
parallel -j 20 --eta < cmd_parallel_alignment_bppalnscore.sh
```

We then extract the selected sites and filter missing data
```r
file <- "cmd_parallel_alignment_bppseqman.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("bppseqman param=SeqMan.bpp \"DATA=", gname, "\"\n", sep = "", file = file, append = TRUE)
}
```
```bash
mkdir Alignments_filtered
parallel -j 20 --eta < cmd_parallel_alignment_bppseqman.sh
```

## Creation of the data matrix

We concatenate all filtered alignments and write a partition file:
```r
library(ape)
concatAln <- NULL
unlink("SuperAlignment.part")
pos <- 1
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  l <- strsplit(gname, "|", fixed = TRUE)
  gname2 <- l[[1]][4]
  file <- paste("Alignments_filtered/", gname, ".aln_filtered.fasta", sep = "")
  aln <- as.matrix(read.FASTA(file, type="AA"))
  cat("LG+G, ", gname2, "=", pos, "-", pos + ncol(aln) - 1, "\n", sep = "", file = "SuperAlignment.part", append = TRUE)
  if (is.null(concatAln)) {
    concatAln <- aln
  } else {
    concatAln <- cbind(concatAln, aln, check.names = TRUE)
  }
  pos <- pos + ncol(aln)
  cat("Current alignment size is", ncol(concatAln), "\n")
}

# 250 species, 556 genes, 278298 selected positions

# We rename the species for RaxML:
tmp <- data.frame(Code = paste("taxon", 1:nrow(concatAln), sep = ""), Species = rownames(concatAln))
write.table(tmp, file = "SpeciesCodes.tsv", row.names = FALSE)
rownames(concatAln) <- paste("taxon", 1:nrow(concatAln), sep = "")
write.FASTA(concatAln, file = "SuperAlignment.fasta")
```

## Build a Maximum Likelihood phylogeny

We make a ML tree with RaxML
```bash
./raxml-ng --check --msa SuperAlignment.fasta --model LG+G
./raxml-ng --check --msa SuperAlignment.fasta --model SuperAlignment.part

./raxml-ng --parse --msa SuperAlignment.fasta --model LG+G
mv SuperAlignment.fasta.raxml.rba SuperAlignment.fasta.raxml_nopart.rba
./raxml-ng --parse --msa SuperAlignment.fasta --model SuperAlignment.part
```

* Estimated memory requirements                : 79009 MB

* Recommended number of threads / MPI processes: 156

We run RaxML-NG on the binary data, which already contains the model specification.
We compare two models based on LG+G. In the "nopart" model, the data is simply concatenated.
In the other model, each gene is a partition and has its own branch scaling parameter.
Other default options are used, 10 random + 10 parsimony starting tree.

On the cluster:
```bash
sbatch runRaxML-NG-nopart.sh
sbatch runRaxML-NG.sh
```

Get all trees:
```bash
cat SuperAlignment.raxml_nopart.raxml.mlTrees > mltrees.dnd
cat SuperAlignment.raxml.raxml.mlTrees >> mltrees.dnd

./raxml-ng --rfdist --tree mltrees.dnd --prefix RF
```
2 Topologies
```bash
./raxml-ng --rfdist --tree SuperAlignment.raxml_nopart.raxml.mlTrees --prefix RF.nopart
./raxml-ng --rfdist --tree SuperAlignment.raxml.raxml.mlTrees --prefix RF.part
```
The two models converge towad a distinct tree.

As a comparison, we use FastTree:
```bash
~/.local/bin/FastTree -lg < SuperAlignment.fasta > SuperAlignment.fasttree.dnd
```

ML crashed on Mac, we try a simple ME tree:
```bash
~/.local/bin/FastTree -lg -noml < SuperAlignment.fasta > SuperAlignment.fasttree_noml.dnd
```

```bash
cat SuperAlignment.fasttree_noml.dnd > fast+mltrees.dnd
cat SuperAlignment.raxml_nopart.raxml.mlTrees >> fast+mltrees.dnd
cat SuperAlignment.raxml.raxml.mlTrees >> fast+mltrees.dnd

./raxml-ng --rfdist --tree fast+mltrees.dnd --prefix RF.fasttree
```
3 topologies. FastTree leads to a different topology.

Final likelihoods (see log files):

```
No partition:
Final LogLikelihood: -33272599.743802

AIC score: 66546195.487603 / AICc score: 66546197.276700 / BIC score: 66551442.633220
Free parameters (model + branch lengths): 498

with partition:
Final LogLikelihood: -33173574.548150

AIC score: 66350365.096300 / AICc score: 66350383.798164 / BIC score: 66367307.686965
Free parameters (model + branch lengths): 1608
```

```r
ll1 <- -33272599.743802
ll2 <- -33173574.548150
pchisq(2*(ll2-ll1), df = 1608-498, lower.tail = FALSE) #0
```

The partition model is clearly a better fit.

## Bootstrap analyses

```bash
mkdir Bootstrap
runRaxMLNG-bootstrap.sh
cat Bootstrap/*.bootstraps > allbootstraps.dnd
./raxml-ng --bsconverge --bs-trees allbootstraps.dnd --prefix BSConverge --seed 42 --threads 1 --bs-cutoff 0.01
```
Check run failures:
```
for i in  {1..200}; do
  if [[ -f Bootstrap/SuperAlignment.fasta.raxml.bootstrap$i.raxml.bootstraps ]];
  then
    "";
  else
    echo $i;
  fi;
done
```
There are five runs to restart: 168,170,171,172,194 (because of overthreading)

Create a bootstrap tree with all 200 replicates:
```
./raxml-ng --support --tree SuperAlignment.raxml.raxml.bestTree --bs-trees allbootstraps.dnd --bs-metric fbp,tbe --prefix SuperAlignment.bootstrap --threads 2 

```

Translate species names:
```r
library(ape)
tln <- read.table("SpeciesCodes.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment.fasttree.dnd")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.fasttree.tln.dnd")

tree <- read.tree("SuperAlignment.raxml_nopart.raxml.bestTree")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.raxml_nopart.tln.dnd")

tree <- read.tree("SuperAlignment.raxml.raxml.bestTree")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.raxml.tln.dnd")

tree <- read.tree("SuperAlignment.bootstrap.raxml.supportFBP")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.bootstrap.raxml.supportFBP.tln.dnd")

tree <- read.tree("SuperAlignment.bootstrap.raxml.supportTBE")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.bootstrap.raxml.supportTBE.tln.dnd")
```

To assess the consistency of genes, we estimate one tree per gene independently (without bootstrap).
We first need to translate all species names using the same convention:

```r
require(ape)
tln <- read.table("SpeciesCodes.tsv", header = TRUE)
rownames(tln) <- tln$Species

dir.create("Alignments_filtered_renamed") 
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  l <- strsplit(gname, "|", fixed = TRUE)
  gname2 <- l[[1]][4]
  file <- paste("Alignments_filtered/", gname, ".aln_filtered.fasta", sep = "")
  aln <- as.matrix(read.FASTA(file, type="AA"))
  rownames(aln) <- tln[rownames(aln), "Code"]
  write.FASTA(aln, file = paste("Alignments_filtered_renamed/", gname, ".aln_filtered_renamed.fasta", sep = ""))
}

```

We print the list of genes to be used by SLURM:
```r
geneNames <- genes
for (i in 1:length(genes)) {
  geneNames[i] <- substring(geneNames[i], 1, nchar(geneNames[i]) - 7)
}
write.table(data.frame(Genes=geneNames), file="AllGenes.txt", col.names = FALSE, row.names = FALSE, quote = FALSE)
```
```bash
sbatch runRaxMLNG-genes.sh
```

Get all estimated trees:
```r
require(ape)
trees <- list()
for (gene in genes) {
  gName <- substring(gene, 1, nchar(gene) - 7)
  print(gName)
  trees[[gName]] <- read.tree(paste("Phylogenies_raxml/", gName, "/", gName, ".raxml.bestTree", sep = ""))
}
class(trees) <- "multiPhylo"
write.tree(trees, file = "AllRaxMLBestTrees.dnd")
plot(consensus(trees, p=0.5))
```
Compute support values based on gene trees:
```bash
./raxml-ng --support --tree SuperAlignment.raxml.raxml.bestTree --bs-trees AllRaxMLBestTrees.dnd --bs-metric fbp,tbe --prefix SuperAlignment.genes --threads 2 

# With bppConsense, to get raw counts and not percentages:
bppconsense param=Consense.bpp
```

```r
library(ape)
tln <- read.table("SpeciesCodes.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment.genes.raxml.supportFBP")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.genes.raxml.supportFBP.tln.dnd")

tree <- read.tree("SuperAlignment.genes.raxml.supportTBE")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment.genes.raxml.supportTBE.tln.dnd")

tree <- read.tree("SuperAlignment_withGeneNumbers.dnd")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment_withGeneNumbers.tln.dnd")

```

Figures in `Figures.R`.


# Second approach: we optimize species close to *P. destructans*

## Get candidate genes and species

```r
allCounts <- read.table("GeneCounts.tsv.gz", sep = "\t", header = TRUE, check.names = FALSE)

# Check species based on the rRNA tree:
species <- c(
 grep("Pseudogymnoascus", allCounts$Taxon, fixed = TRUE, value = TRUE),
 grep("Geomyces", allCounts$Taxon, fixed = TRUE, value = TRUE),
 grep("Holwaya", allCounts$Taxon, fixed = TRUE, value = TRUE),
 grep("Epiglia", allCounts$Taxon, fixed = TRUE, value = TRUE),
 grep("Mniaecia", allCounts$Taxon, fixed = TRUE, value = TRUE)
)

subCounts <- subset(allCounts, Taxon %in% species)
row.names(subCounts) <- subCounts$Taxon
subCounts$Taxon <- NULL
apply(subCounts, 1, function(x) sum(x == 1, na.rm = TRUE))

subCounts2 <- subCounts[which(apply(subCounts, 1, function(x) sum(x == 1, na.rm = TRUE)) > 500),]

selCounts <- read.table(file="GeneCountsSelected.tsv.gz", sep = "\t", check.names = FALSE)

sum(row.names(subCounts) %in% row.names(selCounts)) # 3 species

subCounts3 <- subCounts2[!row.names(subCounts2) %in% row.names(selCounts),]
# Discard dupliacted genes;
subCounts3[subCounts3 > 1] <- NA

subCounts4 <- subCounts3[, apply(subCounts3, 2, function(x) all(!is.na(x)))]

# Now merge the two:
genes <- intersect(colnames(subCounts4), colnames(selCounts)) # 212 genes

selCounts2 <- rbind(selCounts[,genes], subCounts4[,genes])
write.table(selCounts2, file=gzfile("GeneCountsSelected2.tsv.gz"), sep = "\t", row.names = TRUE)
```

## Extract sequences

```r
dat <- read.table(file="GeneCountsSelected2.tsv.gz", sep = "\t", check.names = FALSE)
genes <- colnames(dat)
species <- rownames(dat)
require(seqinr)
queries <- read.fasta("../Genome/Pseudest1_GeneCatalog_proteins_20170919.aa.fasta", seqtype = "AA")

dir.create("Sequences2")
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  seqfile = paste("Sequences2/", gname, ".fasta", sep = "")
  cat("Building sequence file for",  gene, "in", seqfile, "\n")
  tryCatch(blastOut <- read.table(paste("BlastOut/Split/", gene, sep = ""), sep = "\t", stringsAsFactors = FALSE, comment.char = "", quote = ""), warning = function(x) { stop(paste("Problem with file", gene, "\n")) })
  l <- strsplit(blastOut$V9, ";")
  # Remove duplicated sequences (as a double check)
  tbl <- table(unlist(l))
  dupSeqs <- names(tbl[tbl > 1])
  sequences <- list()
  for (i in 1:length(l)) {
    for (j in l[[i]]) {
      if (!j %in% dupSeqs) {
        sequences[[j]] <- blastOut[i, 13]
      }
    }
  } #Note: if we wanted to include duplicated sequences, we would need to keep track of the unique seq ids along with taxon ids
  
  # Now extract sequences (including P. destructans):
  unlink(seqfile)
  for (sp in species) {
    if (! sp %in% names(sequences)) {
      stop(paste("Sequence not found :(", sp))
    } else {
      cat(paste(">", gsub(" ", "_", sp, fixed = TRUE), "\n", sequences[[sp]], "\n", sep = ""), file = seqfile, append = TRUE)    
    }
  }
}

```

## Align sequences

### Align with Muscle

```r
file <- "cmd_parallel_alignment2_muscle.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("muscle -in \"Sequences2/", gname, ".fasta\" -out \"Alignments2_muscle/", gname, ".aln_muscle.fasta\"\n", sep = "", file = file, append = TRUE)
}
```

Align all genes using muscle
```bash
mkdir Alignments2_muscle
parallel -j 20 --eta < cmd_parallel_alignment2_muscle.sh
```

### Align with ClustalO

```r
file <- "cmd_parallel_alignment2_clustalo.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("clustalo -i \"Sequences2/", gname, ".fasta\" -o \"Alignments2_clustalo/", gname, ".aln_clustalo.fasta\" -v\n", sep = "", file = file, append = TRUE)
}
```
Align all genes using clustalo. We only run one at a time as clustalo is multithreaded and uses all available cores
```bash
mkdir Alignments2_clustalo
parallel -j 1 --eta < cmd_parallel2_alignment_clustalo.sh
```

### Make a consensus alignment

Then we compare the two alignments and keep only consistent positions (consensus alignment):
```r
file <- "cmd_parallel_alignment2_bppalnscore.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("bppalnscore param=AlnScore2.bpp \"DATA=", gname, "\"\n", sep = "", file = file, append = TRUE)
}
```
```bash
mkdir Alignments2_consensus
parallel -j 20 --eta < cmd_parallel_alignment2_bppalnscore.sh
```

### Filter the sites

We then extract the selected sites and filter missing data
```r
file <- "cmd_parallel_alignment2_bppseqman.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("bppseqman param=SeqMan2.bpp \"DATA=", gname, "\"\n", sep = "", file = file, append = TRUE)
}
```
```bash
mkdir Alignments2_filtered
parallel -j 20 --eta < cmd_parallel_alignment2_bppseqman.sh
```

## Generate the data matrix

We concatenate all filtered alignments and write a partition file:
```r
library(ape)
concatAln <- NULL
unlink("SuperAlignment2.part")
pos <- 1
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  l <- strsplit(gname, "|", fixed = TRUE)
  gname2 <- l[[1]][4]
  file <- paste("Alignments2_filtered/", gname, ".aln_filtered.fasta", sep = "")
  aln <- as.matrix(read.FASTA(file, type="AA"))
  cat("LG+G, ", gname2, "=", pos, "-", pos + ncol(aln) - 1, "\n", sep = "", file = "SuperAlignment2.part", append = TRUE)
  if (is.null(concatAln)) {
    concatAln <- aln
  } else {
    concatAln <- cbind(concatAln, aln, check.names = TRUE)
  }
  pos <- pos + ncol(aln)
  cat("Current alignment size is", ncol(concatAln), "\n")
}

# 268 species, 212 genes, 90596 selected positions

# We rename the species for RaxML:
tmp <- data.frame(Code = paste("taxon", 1:nrow(concatAln), sep = ""), Species = rownames(concatAln))
write.table(tmp, file = "SpeciesCodes2.tsv", row.names = FALSE)
rownames(concatAln) <- paste("taxon", 1:nrow(concatAln), sep = "")
write.FASTA(concatAln, file = "SuperAlignment2.fasta")
```

## Build a maximum likelihood phylogeny

We make a ML tree with RaxML
```bash
./raxml-ng --check --msa SuperAlignment2.fasta --model LG+G
./raxml-ng --check --msa SuperAlignment2.fasta --model SuperAlignment2.part

./raxml-ng --parse --msa SuperAlignment2.fasta --model LG+G # 64 threads recommended, 29GB
mv SuperAlignment2.fasta.raxml.rba SuperAlignment2.fasta.raxml_nopart.rba
./raxml-ng --parse --msa SuperAlignment2.fasta --model SuperAlignment2.part # 66 threads recommended, 30GB
```

On the cluster:
```bash
sbatch runRaxML-NG-nopart2.sh
sbatch runRaxML-NG2.sh
```

Final likelihoods (see log files):

No partition:

Final LogLikelihood: -11372866.286907

AIC score: 22746800.573814 / AICc score: 22746806.491784 / BIC score: 22751864.678529
Free parameters (model + branch lengths): 534

With partitions:

Final LogLikelihood: -11337163.383175

AIC score: 22676238.766350 / AICc score: 22676257.801220 / BIC score: 22685304.841457
Free parameters (model + branch lengths): 956



Translate species names:
```r
library(ape)
tln <- read.table("SpeciesCodes2.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment2.raxml.raxml.bestTree")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment2.raxml.tln.dnd")

```

## Bootstrap analyses

```bash
mkdir Bootstrap2
sbatch runRaxMLNG-bootstrap2.sh
cat Bootstrap2/*.bootstraps > allbootstraps2.dnd
./raxml-ng --bsconverge --bs-trees allbootstraps2.dnd --prefix BSConverge2 --seed 42 --threads 1 --bs-cutoff 0.01
```

Check run failures:
```
for i in  {1..100}; do
  if [[ ! -f Bootstrap2/SuperAlignment2.fasta.raxml.bootstrap$i.raxml.bootstraps ]];
  then
    echo $i;
  fi;
done
```

Create a bootstrap tree with all 100 replicates:
```
./raxml-ng --support --tree SuperAlignment2.raxml.raxml.bestTree --bs-trees allbootstraps2.dnd --bs-metric fbp,tbe --prefix SuperAlignment2.bootstrap --threads 2 
```

Translate species names:
```r
library(ape)
tln <- read.table("SpeciesCodes2.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment2.bootstrap.raxml.supportFBP")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment2.bootstrap.raxml.supportFBP.tln.dnd")

tree <- read.tree("SuperAlignment2.bootstrap.raxml.supportTBE")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment2.bootstrap.raxml.supportTBE.tln.dnd")
```

`Figures2.R`: generate figures of the phylogeny.


# Third approach: maximize the number of genes and species, including missing data

## Get candidate genes and species

```r
allCounts <- read.table("GeneCounts.tsv.gz", sep = "\t", header = TRUE, check.names = FALSE)

row.names(allCounts) <- allCounts$Taxon
allCounts$Taxon <- NULL

# We ignore all duplicated genes:
allCounts2 <- allCounts
allCounts2[allCounts2 > 1] <- NA

# We discard species with too little genes:
nbGenes <- apply(allCounts2, 1, function(x) sum(!is.na(x)))
require(ggplot2)
p.hist.g <- ggplot(data.frame(NbGenes = nbGenes), aes(x = NbGenes)) + geom_histogram() + scale_x_log10()
p.hist.g

# => We discard species with less than 1000 genes:
selectedSpecies <- names(nbGenes[nbGenes >= 1000])
allCounts3 <- allCounts2[selectedSpecies,]
# => 1257 species!

# Now we look for genes which are present in enough species:
nbSpecies <- apply(allCounts3, 2, function(x) sum(!is.na(x)))
p.hist.s <- ggplot(data.frame(NbSpecies = nbSpecies), aes(x = NbSpecies)) + geom_histogram() + scale_x_log10()
p.hist.s

# Rarefaction curve:
x <- seq(0, 1256, by = 1)
y <- Vectorize(function(x) sum(nbSpecies >= x))(x)
plot(y~x, type = "l", xlab = "Minimum number of species", ylab = "Number of genes")

# We allow for 20% missing data:
threshold <- nrow(allCounts3) * 0.8
abline(v=threshold, col="orange")

# We retrieve the corresponding genes:
selectedGenes <- names(nbSpecies[nbSpecies >= threshold])
allCounts4 <- allCounts3[, selectedGenes] 
# 1257 species, 1146 genes


write.table(allCounts4, file=gzfile("GeneCountsSelected3.tsv.gz"), sep = "\t", row.names = TRUE)
```

## Extract sequences

```r
dat <- read.table(file="GeneCountsSelected3.tsv.gz", sep = "\t", check.names = FALSE)
genes <- colnames(dat)
species <- rownames(dat)
require(seqinr)
queries <- read.fasta("../Genome/Pseudest1_GeneCatalog_proteins_20170919.aa.fasta", seqtype = "AA")

dir.create("Sequences3")
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  seqfile = paste("Sequences3/", gname, ".fasta", sep = "")
  cat("Building sequence file for",  gene, "in", seqfile, "\n")
  tryCatch(blastOut <- read.table(paste("BlastOut/Split/", gene, sep = ""), sep = "\t", stringsAsFactors = FALSE, comment.char = "", quote = ""), warning = function(x) { stop(paste("Problem with file", gene, "\n")) })
  l <- strsplit(blastOut$V9, ";")
  # Remove duplicated sequences (as a double check)
  tbl <- table(unlist(l))
  dupSeqs <- names(tbl[tbl > 1])
  sequences <- list()
  for (i in 1:length(l)) {
    for (j in l[[i]]) {
      if (!j %in% dupSeqs) {
        sequences[[j]] <- blastOut[i, 13]
      }
    }
  } #Note: if we wanted to include duplicated sequences, we would need to keep track of the unique seq ids along with taxon ids
  
  # Now extract sequences (including P. destructans):
  unlink(seqfile)
  for (sp in species) {
    if (sp %in% names(sequences)) {
      cat(paste(">", gsub(" ", "_", sp, fixed = TRUE), "\n", sequences[[sp]], "\n", sep = ""), file = seqfile, append = TRUE)    
    }
  }
}

```

## Align sequences

### Using Muscle

```r
file <- "SLURM_alignment3_muscle.sh"
unlink(file)
cat("#!/bin/bash\n", file = file, append = TRUE)
cat("#SBATCH --partition=standard\n", file = file, append = TRUE)
cat("#SBATCH --nodes=1\n", file = file, append = TRUE)
cat("#SBATCH --time=48:00:00\n", file = file, append = TRUE)
cat("#SBATCH --output=log/ali3_muscle.%J.out\n", file = file, append = TRUE)
cat("#SBATCH --error=log/ali3_muscle.%J.err\n", file = file, append = TRUE)
cat("#SBATCH --ntasks=1\n", file = file, append = TRUE)
cat("#SBATCH --mail-type=BEGIN\n", file = file, append = TRUE)
cat("#SBATCH --mail-type=END\n", file = file, append = TRUE)
cat("#SBATCH --mail-user=dutheil@evolbio.mpg.de\n", file = file, append = TRUE)
cat("#SBATCH --mem=12G\n", file = file, append = TRUE)
cat("#SBATCH --job-name=ali3_muscle\n", file = file, append = TRUE)
cat("#SBATCH --array=1-1146\n", file = file, append = TRUE)

counter <- 0
for (gene in genes) {
  counter <- counter + 1
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("if [ $SLURM_ARRAY_TASK_ID == ", counter, " ]; then\n", sep = "", file = file, append = TRUE)
  cat("  muscle -in \"Sequences3/", gname, ".fasta\" -out \"Alignments3_muscle/", gname, ".aln_muscle.fasta\"\n", sep = "", file = file, append = TRUE)
  cat("fi\n", sep = "", file = file, append = TRUE)
}
```

Align all genes using muscle
```bash
mkdir log
mkdir Alignments3_muscle
sbatch SLURM_alignment3_muscle.sh
```

### Using Clustal Omega

We use 8 threads per run
```r
file <- "SLURM_alignment3_clustalo.sh"
unlink(file)
cat("#!/bin/bash\n", file = file, append = TRUE)
cat("#SBATCH --partition=standard\n", file = file, append = TRUE)
cat("#SBATCH --nodes=1\n", file = file, append = TRUE)
cat("#SBATCH --time=48:00:00\n", file = file, append = TRUE)
cat("#SBATCH --output=log/ali3_clustalo.%J.out\n", file = file, append = TRUE)
cat("#SBATCH --error=log/ali3_clustalo.%J.err\n", file = file, append = TRUE)
cat("#SBATCH --ntasks=1\n", file = file, append = TRUE)
cat("#SBATCH --cpus-per-task=8\n", file = file, append = TRUE)
cat("#SBATCH --mail-type=BEGIN\n", file = file, append = TRUE)
cat("#SBATCH --mail-type=END\n", file = file, append = TRUE)
cat("#SBATCH --mail-user=dutheil@evolbio.mpg.de\n", file = file, append = TRUE)
cat("#SBATCH --mem=12G\n", file = file, append = TRUE)
cat("#SBATCH --job-name=ali3_clustalo\n", file = file, append = TRUE)
cat("#SBATCH --array=1-1146\n", file = file, append = TRUE)

counter <- 0
for (gene in genes) {
  counter <- counter + 1
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("if [ $SLURM_ARRAY_TASK_ID == ", counter, " ]; then\n", sep = "", file = file, append = TRUE)
  cat("  clustalo --threads=8 -i \"Sequences3/", gname, ".fasta\" -o \"Alignments3_clustalo/", gname, ".aln_clustalo.fasta\" -v\n", sep = "", file = file, append = TRUE)
  cat("fi\n", sep = "", file = file, append = TRUE)
}
```
Align all genes using clustalo. 
```bash
mkdir Alignments3_clustalo
sbatch SLURM_alignment3_clustalo.sh
```

### Make a consensus alignment

Then we compare the two alignments and keep only consistent positions (consensus alignment):
```r
file <- "cmd_parallel_alignment3_bppalnscore.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("bppalnscore param=AlnScore3.bpp \"DATA=", gname, "\"\n", sep = "", file = file, append = TRUE)
}
```
```bash
mkdir Alignments3_consensus
parallel -j 20 --eta < cmd_parallel_alignment3_bppalnscore.sh
```

### Extract selected site and filter missing data

```r
file <- "cmd_parallel_alignment3_bppseqman.sh"
unlink(file)
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  cat("bppseqman param=SeqMan3.bpp \"DATA=", gname, "\"\n", sep = "", file = file, append = TRUE)
}
```
```bash
mkdir Alignments3_filtered
parallel -j 20 --eta < cmd_parallel_alignment3_bppseqman.sh
```

## Generate the data matrix

We concatenate all filtered alignments and write a partition file:
```r
library(ape)
concatAln <- NULL
unlink("SuperAlignment3.part")
pos <- 1
for (gene in genes) {
  gname <- substring(gene, 1, nchar(gene) - 7)
  l <- strsplit(gname, "|", fixed = TRUE)
  gname2 <- l[[1]][4]
  file <- paste("Alignments3_filtered/", gname, ".aln_filtered.fasta", sep = "")
  aln <- as.matrix(read.FASTA(file, type="AA"))
  cat("LG+G, ", gname2, "=", pos, "-", pos + ncol(aln) - 1, "\n", sep = "", file = "SuperAlignment3.part", append = TRUE)
  if (is.null(concatAln)) {
    concatAln <- aln
  } else {
    concatAln <- cbind(concatAln, aln, check.names = TRUE, fill.with.Xs = TRUE)
  }
  pos <- pos + ncol(aln)
  cat("Current alignment size is", ncol(concatAln), "\n")
}

dim(concatAln)
# 1257 species, 1146 genes, 461596 selected positions

# We rename the species for RaxML:
tmp <- data.frame(Code = paste("taxon", 1:nrow(concatAln), sep = ""), Species = rownames(concatAln))
write.table(tmp, file = "SpeciesCodes3.tsv", row.names = FALSE)
rownames(concatAln) <- paste("taxon", 1:nrow(concatAln), sep = "")
write.FASTA(concatAln, file = "SuperAlignment3.fasta")
```

## Build a tree with fasttree

Run version 2.1.11 DoublePrecision no SSE3 (SSE3 version leads to a numerical error, possibly because of very similar sequences)
```bash
sbatch runFastTree3.sh
```

Translate species names:
```r
library(ape)
tln <- read.table("SpeciesCodes3.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment3.fasttree.dnd")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment3.fasttree.tln.dnd")
```

### Filter strains

We run PhySamp to remove taxa which are more than 3% similar in sequences, keeping the one with most data:
```r
bppphysamp param=PhySamp.bpp
```
The 3% threshold removes a lot of the multiple S. cerevisiae strains.
Increasing the threshold removes the Pd genome, so this is the level of diversity in the Pd group, which we use as a threshold for the rest of the phylogeny.
The filtered tree still has 817 sequences.

Translate species names and check sequence clusters:
```r
library(ape)
tln <- read.table("SpeciesCodes3.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment3.fasttree.sampled03.dnd")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment3.fasttree.sampled03.tln.dnd")

logfile <- read.table("SuperAlignment3.sampled03.log", header = TRUE)
logfile$For <- tln[logfile$For, "Species"]
logfile$Discarded <- tln[logfile$Discarded, "Species"]
write.table(logfile, "SuperAlignment3.sampled03.tln.log", row.names = FALSE)

# Note: log file contains info by order of removal
clusters <- list()
for (i in 1:nrow(logfile)) {
  if (is.null(clusters[[logfile$Discarded[i]]])) {
    clusters[[logfile$For[i]]] <- logfile$Discarded[i]
  } else {
    clusters[[logfile$For[i]]] <- c(clusters[[logfile$Discarded[i]]], logfile$Discarded[i])
    clusters[[logfile$Discarded[i]]] <- NULL
  }
}

# Write to file:
f <- "SuperAlignment3.sampled03.clusters.txt"
unlink(f)
for (i in names(clusters)) {
  cat(i, ":", paste(clusters[[i]], collapse = ","), "\n", file = f, append = TRUE)
}
```


## Build a maximum likelihood phylogeny

We make a ML tree with RaxML, using the filter data to reduce computational burden:
```bash
./raxml-ng --check --msa SuperAlignment3.sampled03.fasta --model LG+G
./raxml-ng --check --msa SuperAlignment3.sampled03.fasta --model SuperAlignment3.part

./raxml-ng --parse --msa SuperAlignment3.sampled03.fasta --model LG+G # 250 threads recommended, 457GB
mv SuperAlignment3.sampled03.fasta.raxml.rba SuperAlignment3.sampled03.fasta.raxml_nopart.rba
./raxml-ng --parse --msa SuperAlignment3.sampled03.fasta --model SuperAlignment3.part # 251 threads recommended, 459GB
```

On the cluster:
```bash
sbatch runRaxMLNG-nopart3.sh
sbatch runRaxMLNG3.sh
```
Note: this takes too much time!

Use the sampled FastTree tree as starting tree:
```bash
sbatch runRaxMLNG-nopart3-fasttree.sh
sbatch runRaxMLNG3-fasttree.sh
```

Final likelihoods (see log files):

No partition:
```
Final LogLikelihood: -199223745.442874

AIC score: 398450754.885748 / AICc score: 398450766.473881 / BIC score: 398468776.156525
Free parameters (model + branch lengths): 1632
```
With partitions:
```
Final LogLikelihood: -198682673.285000

AIC score: 397373190.570000 / AICc score: 397373257.805804 / BIC score: 397416499.040579
Free parameters (model + branch lengths): 3922
```


Translate species names:
```r
library(ape)
tln <- read.table("SpeciesCodes3.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment3.fasttree.sampled03.raxml.raxml.bestTree")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment3.fasttree.sampled03.raxml.tln.dnd")

```

Note: some species are indicated as [candida], which leads this to be ignored when parsed by R. We manually replace this by "Candida":
```bash
sed -i '' 's/\[Candida\]/Candida/' SuperAlignment3.fasttree.sampled03.raxml.tln.dnd
```

`Figures3.R`: generate figures of the phylogeny.

# Phylogeny of Pezizomycotina

We then redo the phylogeny with sequences from Pezizomycotina only.

## Extract the subalignment

```r
require(ape)
tree <- read.tree("SuperAlignment3.fasttree.sampled03.raxml.tln.pezizomycotina.dnd")
aln  <- read.aa("SuperAlignment3.sampled03.fasta", format = "fasta")
species <- read.csv("SpeciesCodes3.tsv", sep = " ", header = TRUE)
species$Species <- gsub("(", "-", species$Species, fixed = TRUE) 
species$Species <- gsub(")", "", species$Species, fixed = TRUE) 
species$Species <- gsub("'", "", species$Species, fixed = TRUE) 
selection <- subset(species, Species %in% tree$tip.label)
aln2 <- aln[selection$Code]
write.FASTA(aln2, "SuperAlignment3.sampled03.pezizo.fasta", header = NULL, append = FALSE)
```

## Build a maximum likelihood phylogeny

We make a ML tree with RaxML, using the filter data to reduce computational burden:
```bash
./raxml-ng --check --msa SuperAlignment3.sampled03.pezizo.fasta --model SuperAlignment3.part
./raxml-ng --parse --msa SuperAlignment3.sampled03.pezizo.fasta --model SuperAlignment3.part # 246 threads recommended, 230GB
```

On the cluster:
```bash
sbatch runRaxMLNG3pezizo.sh
```

Final likelihoods (see log files):

```
Final LogLikelihood: -81345305.712741

AIC score: 162696871.425483 / AICc score: 162696914.176969 / BIC score: 162731434.279361
Free parameters (model + branch lengths): 3130
```

## Bootstrap analyses

```bash
mkdir Bootstrap3p
sbatch runRaxMLNG-bootstrap3pezizo.sh
cat Bootstrap3p/*.bootstraps > allbootstraps3p.dnd
./raxml-ng --bsconverge --bs-trees allbootstraps3p.dnd --prefix BSConverge3p --seed 42 --threads 1 --bs-cutoff 0.01
```

Check run failures:
```
for i in  {1..100}; do
  if [[ ! -f Bootstrap3p/SuperAlignment3.sampled03.pezizo.fasta.raxml.bootstrap$i.raxml.bootstraps ]];
  then
    echo $i;
  fi;
done
```
(Needs to rerun 15 and 17.)

Create a bootstrap tree with all 100 replicates:
```
./raxml-ng --support --tree SuperAlignment3.sampled03.pezizo.raxml.raxml.bestTree --bs-trees allbootstraps3p.dnd --bs-metric fbp,tbe --prefix SuperAlignment3p.bootstrap --threads 2 
```

Translate species names:
```r
library(ape)
tln <- read.table("SpeciesCodes3.tsv", header = TRUE)
rownames(tln) <- tln$Code

tree <- read.tree("SuperAlignment3p.bootstrap.raxml.supportFBP")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment3p.bootstrap.raxml.supportFBP.tln.dnd")

tree <- read.tree("SuperAlignment3p.bootstrap.raxml.supportTBE")
tree$tip.label <- tln[tree$tip.label, "Species"]
write.tree(tree, file = "SuperAlignment3p.bootstrap.raxml.supportTBE.tln.dnd")
```


