#!/bin/bash

#SBATCH --job-name=wns-raxml3pezizo
#SBATCH --ntasks=32 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=502G 
#SBATCH --error=raxml_log3p.%J.err
#SBATCH --output=raxml_log3p.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 


./raxml-ng --threads 32 --msa SuperAlignment3.sampled03.pezizo.fasta.raxml.rba --brlen scaled --prefix SuperAlignment3.sampled03.pezizo.raxml --seed 42

