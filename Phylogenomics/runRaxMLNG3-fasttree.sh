#!/bin/bash

#SBATCH --job-name=wns-raxml3-fasttree
#SBATCH --ntasks=32 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=502G 
#SBATCH --error=raxml_fasttree_log3.%J.err
#SBATCH --output=raxml_fasttree_log3.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 


./raxml-ng --threads 32 --msa SuperAlignment3.sampled03.fasta.raxml.rba --tree SuperAlignment3.fasttree.sampled03.dnd --brlen scaled --prefix SuperAlignment3.sampled03.raxml --seed 42

