#!/bin/bash

#SBATCH --job-name=wns-raxml2
#SBATCH --ntasks=32 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=40G 
#SBATCH --error=cluster_log2.%J.err
#SBATCH --output=cluster_log2.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 


./raxml-ng --threads 32 --msa SuperAlignment2.fasta.raxml.rba --brlen scaled --prefix SuperAlignment2.raxml --seed 42

