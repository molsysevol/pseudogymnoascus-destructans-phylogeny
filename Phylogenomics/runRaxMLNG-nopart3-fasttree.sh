#!/bin/bash

#SBATCH --job-name=wns-raxml-nopart3-fasttree
#SBATCH --ntasks=32 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=502G 
#SBATCH --error=raxml-nopart_fasttree_log3.%J.err
#SBATCH --output=raxml-nopart_fasttree_log3.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=standard 


./raxml-ng --threads 32 --msa SuperAlignment3.sampled03.fasta.raxml_nopart.rba --tree SuperAlignment3.fasttree.sampled03.dnd --prefix SuperAlignment3.sampled03.raxml_nopart --seed 42

