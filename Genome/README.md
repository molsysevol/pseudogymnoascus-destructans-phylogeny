The following genome data can be retrieved from the JGI portal (https://mycocosm.jgi.doe.gov/Pseudest1/Pseudest1.home.html):

- Pseudest1_GeneCatalog_proteins_20170919.aa.fasta.gz
- Pseudest1_AssemblyScaffolds.fasta
- Pseudest1_GeneCatalog_20170919.gff3.gz

